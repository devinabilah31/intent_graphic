package com.example.asus_rog.belajarinputgambar;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.widget.ImageView;

import static android.graphics.Bitmap.Config.ARGB_8888;

public class MainActivity extends Activity {

    ImageView ourView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        draw();

         setContentView(ourView);

    }

    public void draw(){


        Bitmap blankBitmap;
        blankBitmap = Bitmap.createBitmap(600,600, ARGB_8888);
        Canvas canvas;
        canvas = new Canvas(blankBitmap);

        ourView = new ImageView(this);
        ourView.setImageBitmap(blankBitmap);

        Paint paint;
        paint = new Paint();

        // Memberikan warna kanvas
        canvas.drawColor(Color.argb(100, 46, 139, 87));

        // Mengatur warna
        paint.setColor(Color.argb(100,  225, 80, 182));
        // We can change this around as well


        Bitmap bitmapGambar;
        // insialisasi gambar
        bitmapGambar = BitmapFactory.decodeResource(this.getResources(), R.drawable.segitiga);
        // Posisi gambar
        canvas.drawBitmap(bitmapGambar, 30, 25, paint);

        // Membuat Garis
        canvas.drawLine(200,500,500,500,paint);

        // membuat Tulisan
        canvas.drawText("Look at this gaes!!", 100, 500, paint);

        // membuat lingkaran
        canvas.drawCircle(300,100,300,paint);

        // mengubah warna
        paint.setColor(Color.argb(100,  50, 200, 110));

        // membuat kotak
        canvas.drawRect(0,400,350,400,paint);

        }
}